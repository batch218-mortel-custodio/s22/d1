/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// Mutator methods - methods or funcs that mutate or change an array

console.log("==Mutator Methods==");
console.log("---------------");
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Katol"];

// push() method - ADDS an element to the end of an array AND RETURNS the new array's length
// syntax: arrayName.push[newElement];

console.log("=>push()<=");
console.log("fruits array: ");
console.log(fruits);

fruits[fruits.length] = "Mango";

fruits.push("Cocomelon");
fruits.push("DaCocomelon");

console.log("Size/length of fruits array: "+ fruits.length);
console.log(fruits);

/////////////////////////////////////////////


fruits.push("avocado", "guava"); /// push() can PUSH MULTIPLE items unlike old way of adding
console.log(fruits);

//////////////////////////
function addMultipleFruits(fruit1, fruit2, fruit3){
    fruits.push(fruit1, fruit2, fruit3);
    console.log(fruits);
}

addMultipleFruits("Durian", "Atis", "Tomato");

//////////////////////////////////////////////////

// pop() method - REMOVES the last element AND Returns the new array's length

    
console.log("----------------------/////////---------------------");
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from POP: ");
console.log(fruits);


// unshift()
/*
    - Add one or more elements at the BEGINNING of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log("----------------------/////////---------------------");
fruits.unshift("Lime", "NANANA");
console.log("Mutated array from UNSHIFT: ");
console.log(fruits);



// shift() - REMOVES an element at the beg of an array

console.log("----------------------/////////---------------------");

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from SHIFT: ");
console.log(fruits);

// SPLICE() - simultaneously remove the element from a specified index number and adds new elements

//syntax: arrayName.splice(startingIndex, deleteCount)
;
console.log("----------------------/////////---------------------");

fruits.splice(1, 2, "Lime", "Cherry", "Tomato");
console.log("Mutated array from SPLICE: ");
console.log(fruits);



// SORT()
/*
    -Rearranges the array elements in alphanumeric order
    -Syntax:
        - arrayName.sort();
*/

console.log("----------------------/////////---------------------");

fruits.sort();
console.log("Mutated array from SORT: ");
console.log(fruits);



// REVERSE()

console.log("----------------------/////////---------------------");

fruits.reverse();
console.log("Mutated array from REVERSE: ");
console.log(fruits);



// Non-mutator - are functions that do not modify or change an array after they are created

console.log("----------------------/////////---------------------");

console.log("NON-MUTATOR METHODS ");

// indexOf() methoddd

/*
    - Returns the index of the first matching element found in an array.
    - If NO MATCH was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

// index value    0      1     2     3     4     5     6    7
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf('PH')): "+firstIndex);

let secondIndex = countries.indexOf("PH", 2); // with a specified index to START
console.log("Result of indexOf:  "+secondIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf('BR'): "+invalidCountry);

console.log("----------------------/////////---------------------");


// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the LAST ELEMENT proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf('PH'): " +lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf('PH', 4): " +lastIndexStart);


console.log("----------------------/////////---------------------");

// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/


    console.log("Original countries array:");
    console.log(countries);


/* index value   -8   -7     -6    -5    -4    -3    -2    -1
/* index value    0      1     2     3     4     5     6    7
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]*/

let sliceArrayA = countries.slice(2);
    console.log("Result from SLICE(2):");
    console.log(sliceArrayA);

let sliceArrayB = countries.slice(3,5); //last value on the range (-1) okayyyy???
    console.log("Result from SLICE(3,5):");
    console.log(sliceArrayB);


let sliceArrayC = countries.slice(-3);
    console.log("Result from slice method:");
    console.log(sliceArrayC);   // PH, FRE, DE

console.log(countries);



console.log("----------------------/////////---------------------");


// toString
/*
    - Returns/CONVERTS an array INTO a string, separated by commas(removing the brackets).
    - Syntax 
        arrayName.toString();
*/

// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
console.log(stringArray[2]);
console.log(typeof stringArray); //to check if the array is converted to string.